from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import instaloader
from instaloader import Profile
from tqdm import tqdm
import json

USER = "haianh230797"
PASSWORD = "F0urworld6@"


def start_crawl(profile_name):
    post_list = []
    L = instaloader.Instaloader()
    L.login(USER, PASSWORD)
    profile = Profile.from_username(L.context, profile_name)
    all_posts = profile.get_posts()
    postcount = 0
    try:

        for post in tqdm(all_posts):
            try:
                caption = post.caption
                date_time = post.date_local
                mediaid = post.mediaid
                owner = post.owner_username
                owner_id = post.owner_id
                likes = post.likes
                media_count = post.mediacount
                pictures = []
                for node in post.get_sidecar_nodes():
                    pictures.append(node.display_url)
                if len(pictures) == 0:
                    pictures.append(post.url)

                post_dict = {
                    "caption": caption,
                    "pictures": pictures,
                    "date_time": date_time,
                    "mediaid": mediaid,
                    "owner": owner,
                    "owner_id": owner_id,
                    "likes": likes,
                    "media_count": media_count
                }
                post_list.append(post_dict)
                postcount += 1
                print(postcount)
                # L.download_post(post, target=profile.username)
            except Exception as e:
                print(e)
    except:
        with open("resume_information.json", 'w') as file:
            file.write(all_posts.freeze())

    with open('bemy.json', 'w') as f:
        json.dump(post_list, f)


if __name__ == '__main__':
    profile_name = "bemycoffee.hn"
    start_crawl(profile_name)
